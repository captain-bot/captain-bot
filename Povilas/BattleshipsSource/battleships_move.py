from random import randint
from random import choice


def calculateMove(gamestate):
    if gamestate["Round"] == 0:  # If we are in the ship placement round
        # move = exampleShipPlacement()  # Does not take land into account
        move = deployRandomly(gamestate)  # Randomly place your ships
    else:  # If we are in the ship hunting round
        #move = chooseRandomValidTarget(gamestate)  # Randomly fire at valid sea targets
        move = sinkTheShips(gamestate)
    return move


# custom code
def sinkTheShips(gamestate):
    print(str(gamestate["OppBoard"]))
    fightingTheShip = False
    nonSunkShip = calcCurrHits(gamestate["OppBoard"])
    if (len(nonSunkShip) != 0):
        fightingTheShip = True

    # if no ships that beed hit but not sunk
    if not fightingTheShip:
        print("random move")
        # randomMove = generateSemiRandomHit(gamestate["OppBoard"])
        randomMove = giveRandomizerSuggestion(gamestate["OppBoard"])
        print(str(randomMove))
        return translateMove(randomMove[0], randomMove[1])


    # pursue the ship
    else:
        # print("We are fighting")
        # print(len(nonSunkShip))
        # hit the ship nearby. 0 Top, 1 Right...
        # select first one

        if len(nonSunkShip) > 1:
            print("ship extrapolation")
            possibleKill = findPossibleSink(nonSunkShip, gamestate["OppBoard"])
            if len(possibleKill) == 0:
                # not a good approach with randomizer, loses targets
                # randomMove = generateSemiRandomHit(gamestate["OppBoard"])
                # return translateMove(randomMove[0], randomMove[1])
                nearbyTarget = seekNearbyShips(nonSunkShip, gamestate["OppBoard"])
                return translateMove(nearbyTarget[0], nearbyTarget[1])
            # print('Possible kills: ' + str(possibleKill))
            coordinates = randint(0, len(possibleKill) - 1)
            return translateMove(possibleKill[coordinates][0], possibleKill[coordinates][1])
        else:
            nearbyTarget = seekNearbyShips(nonSunkShip, gamestate["OppBoard"])
            return translateMove(nearbyTarget[0], nearbyTarget[1])

    return move


# def generateSemiRandomHit(oppBoard):
#     valid = False
#     row = None
#     column = None
#     while not valid:
#         row = randint(0, len(oppBoard) - 1)
#         if row % 2 == 0:
#             column = randint(0, 3) * 2
#         else:
#             column = randint(0, 3) * 2 + 1

#         moveGenerated = {"Row": chr(row + 65),
#                         "Column": (column + 1)}
#         #print('GENERATED MOVE: ' + str(moveGenerated) + str(row) + " " + str(column))

#         #if oppBoard[row][column] == "":  # If the target is sea that hasn't already been guessed...
#         #    valid = True  # ...then the target is valid

#         if checkIfHitReasonable(row, column, oppBoard):
#             valid = True
#     return [row, column]

def seekNearbyShips(nonSunkShip, oppBoard):
    print("Random detection")
    adjacentCells = selectUntargetedAdjacentCell(nonSunkShip[0][0], nonSunkShip[0][1], oppBoard)
    if len(adjacentCells) > 1:
        randomizeMove = randint(0, len(adjacentCells) - 1)
    else:
        randomizeMove = 0
    # print("randomize move " + str(randomizeMove))
    # print(str(adjacentCells))
    row = adjacentCells[randomizeMove][0]
    column = adjacentCells[randomizeMove][1]
    return [row, column]


def calcCurrHits(oppBoard):
    # check if fighting ship
    nonSunkShip = []
    for row in range(8):
        for column in range(8):
            if oppBoard[row][column] == 'H':
                nonSunkShip.append([row, column])
                # print("We are fighting")
    # print('ship hits: ' + str(nonSunkShip))
    return nonSunkShip


def findPossibleSink(shipArray, oppBoard):
    # find all hittable lines
    hitpoints = []
    for index in range(0, len(shipArray)):
        for index2 in range(index + 1, len(shipArray)):
            # vertical
            if (shipArray[index][0] == shipArray[index2][0]):
                # print("ship extrapolation, vetical check pass")
                if checkIfHitPossible(shipArray[index][0], shipArray[index][1] + 1, oppBoard):
                    hitpoints.append([shipArray[index][0], shipArray[index][1] + 1])
                if checkIfHitPossible(shipArray[index][0], shipArray[index][1] - 1, oppBoard):
                    hitpoints.append([shipArray[index][0], shipArray[index][1] - 1])
                if checkIfHitPossible(shipArray[index2][0], shipArray[index2][1] + 1, oppBoard):
                    hitpoints.append([shipArray[index2][0], shipArray[index2][1] + 1])
                if checkIfHitPossible(shipArray[index2][0], shipArray[index2][1] - 1, oppBoard):
                    hitpoints.append([shipArray[index2][0], shipArray[index2][1] - 1])
            # horizontal
            if (shipArray[index][1] == shipArray[index2][1]):
                # print("ship extrapolation, horizontal check pass")
                if checkIfHitPossible(shipArray[index][0] + 1, shipArray[index][1], oppBoard):
                    hitpoints.append([shipArray[index][0] + 1, shipArray[index][1]])
                if checkIfHitPossible(shipArray[index][0] - 1, shipArray[index][1], oppBoard):
                    hitpoints.append([shipArray[index][0] - 1, shipArray[index][1]])
                if checkIfHitPossible(shipArray[index2][0] + 1, shipArray[index2][1], oppBoard):
                    hitpoints.append([shipArray[index2][0] + 1, shipArray[index2][1]])
                if checkIfHitPossible(shipArray[index2][0] - 1, shipArray[index2][1], oppBoard):
                    hitpoints.append([shipArray[index2][0] - 1, shipArray[index2][1]])

                    # print("ship extrapolation, index " + str(index) + " curr\n" + str(hitpoints))

    return hitpoints


def checkIfHitPossible(row, column, oppBoard):
    if row < 0:
        return False
    elif row > 7:
        return False
    # print('row ok')

    if column < 0:
        return False
    elif column > 7:
        return False
    # print('col ok')

    if oppBoard[row][column] == '':
        # print('empty ok')
        return True
    else:
        return False


def checkIfHitReasonable(row, column, oppBoard):
    mainTile = checkIfHitPossible(row, column, oppBoard)
    if not mainTile:
        return False
    # check if ship fits
    okToPlace = False
    shipsLeft = checkShipsLeft(oppBoard)
    shipSize = [5, 4, 3, 3, 2]
    for ships in range(5):
        if shipsLeft[ships] != 0:
            for movement in range(shipSize[ships]):
                print('infinite loop')
                allGood = True
                for pos in range(shipSize[ships]):
                    if not checkIfHitPossible(row - (movement) + pos, column, oppBoard):
                        allGood = False

                for pos in range(shipSize[ships]):
                    if not checkIfHitPossible(row, column - (movement) + pos, oppBoard):
                        allGood = False

                # if at least one ship fits its good
                if allGood:
                    okToPlace = True

    # #to be implemented with max size detextion
    return okToPlace


def giveRandomizerSuggestion(oppBoard):
    suggestions = []
    # choose smallest boat
    shipSize = [5, 4, 3, 3, 2]
    shipsLeft = checkShipsLeft(oppBoard)
    highestBoat = 0
    for i in range(5):
        if shipsLeft[i] == 1:
            highestBoat = i
    minBoatSize = shipSize[i]
    print(str(minBoatSize))

    # one direction
    if minBoatSize == 2:
        for column in range(8):
            for row in range(8):
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row + 1, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column + 1, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row - 1, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column - 1, oppBoard):
                    suggestions.append([row, column])

    if minBoatSize == 3:
        for column in range(8):
            for row in range(8):
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row + 1, column,
                                                                                    oppBoard) and checkIfHitPossible(
                                row + 2, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column + 1,
                                                                                    oppBoard) and checkIfHitPossible(
                        row, column + 2, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row - 1, column,
                                                                                    oppBoard) and checkIfHitPossible(
                                row - 2, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column - 1,
                                                                                    oppBoard) and checkIfHitPossible(
                        row, column - 2, oppBoard):
                    suggestions.append([row, column])

    if minBoatSize >= 4:
        for column in range(8):
            for row in range(8):
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row + 1, column,
                                                                                    oppBoard) and checkIfHitPossible(
                                row + 2, column, oppBoard) and checkIfHitPossible(row + 3, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column + 1,
                                                                                    oppBoard) and checkIfHitPossible(
                        row, column + 2, oppBoard) and checkIfHitPossible(row, column + 3, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row - 1, column,
                                                                                    oppBoard) and checkIfHitPossible(
                                row - 2, column, oppBoard) and checkIfHitPossible(row - 3, column, oppBoard):
                    suggestions.append([row, column])
                if checkIfHitPossible(row, column, oppBoard) and checkIfHitPossible(row, column - 1,
                                                                                    oppBoard) and checkIfHitPossible(
                        row, column - 2, oppBoard) and checkIfHitPossible(row, column - 3, oppBoard):
                    suggestions.append([row, column])

    # apply mesh filter
    valid = False
    while not valid:
        choice = randint(0, len(suggestions) - 1)
        if suggestions[choice][0] % 2 == 0:
            if suggestions[choice][1] % 2 == 0:
                return suggestions[choice]
        else:
            if (suggestions[choice][1] + 1) % 2 == 0:
                return suggestions[choice]

                # print(str(suggestions))
                # return suggestions[]


def checkShipsLeft(oppBoard):
    # 5, 4, 3, 3, 2
    aliveShips = [1, 1, 1, 1, 1]
    for row in range(8):
        for column in range(8):
            if oppBoard[row][column] == 'S0':
                aliveShips[0] = 0
            if oppBoard[row][column] == 'S1':
                aliveShips[1] = 0
            if oppBoard[row][column] == 'S2':
                aliveShips[2] = 0
            if oppBoard[row][column] == 'S3':
                aliveShips[3] = 0
            if oppBoard[row][column] == 'S4':
                aliveShips[4] = 0
    return aliveShips

# =============================================================================
# The code below shows a selection of helper functions designed to make the
# time to understand the environment and to get a game running as short as
# possible. The code also serves as an example of how to manipulate the myBoard
# and oppBoard.


def exampleShipPlacement():
    # The Placement list adds ships in the order that the ships are
    # listed in the game style e.g. 5,4,3,3,2 places the ship of length
    # 5 first, the ship of length 4 second, the ship of length 3 third.
    #
    # This function does not check for any land and, so, should be used
    # with a gamestyle that does not include land.
    move = {"Placement": [
                  {
                    "Row": "A",
                    "Column": 1,
                    "Orientation": "H"
                  },
                  {
                    "Row": "B",
                    "Column": 6,
                    "Orientation": "V"
                  },
                  {
                    "Row": "C",
                    "Column": 1,
                    "Orientation": "H"
                  },
                  {
                    "Row": "D",
                    "Column": 1,
                    "Orientation": "H"
                  },
                  {
                    "Row": "E",
                    "Column": 1,
                    "Orientation": "V"
                  }
               ]
            }
    return move


# Deploys all the ships randomly on a blank board
def deployRandomly(gamestate):
    move = []  # Initialise move as an emtpy list
    orientation = None
    row = None
    column = None
    for i in range(len(gamestate["Ships"])):  # For every ship that needs to be deployed
        deployed = False
        while not deployed:  # Keep randomly choosing locations until a valid one is chosen
            row = randint(0, len(gamestate["MyBoard"]) - 1)  # Randomly pick a row
            column = randint(0, len(gamestate["MyBoard"][0]) - 1)  # Randomly pick a column
            orientation = choice(["H", "V"])  # Randomly pick an orientation
            if deployShip(row, column, gamestate["MyBoard"], gamestate["Ships"][i], orientation, i):  # If ship can be successfully deployed to that location...
                deployed = True  # ...then the ship has been deployed
        move.append({"Row": chr(row + 65), "Column": (column + 1),
                     "Orientation": orientation})  # Add the valid deployment location to the list of deployment locations in move
    return {"Placement": move}  # Return the move


# Returns whether given location can fit given ship onto given board and, if it can, updates the given board with that ships position
def deployShip(i, j, board, length, orientation, ship_num):
    if orientation == "V":  # If we are trying to place ship vertically
        if i + length - 1 >= len(board):  # If ship doesn't fit within board boundaries
            return False  # Ship not deployed
        for l in range(length):  # For every section of the ship
            if board[i + l][j] != "":  # If there is something on the board obstructing the ship
                return False  # Ship not deployed
        for l in range(length):  # For every section of the ship
            board[i + l][j] = str(ship_num)  # Place the ship on the board
    else:  # If we are trying to place ship horizontally
        if j + length - 1 >= len(board[0]):  # If ship doesn't fit within board boundaries
            return False  # Ship not deployed
        for l in range(length):  # For every section of the ship
            if board[i][j + l] != "":  # If there is something on the board obstructing the ship
                return False  # Ship not deployed
        for l in range(length):  # For every section of the ship
            board[i][j + l] = str(ship_num)  # Place the ship on the board
    return True  # Ship deployed


# Randomly guesses a location on the board that hasn't already been hit
def chooseRandomValidTarget(gamestate):
    valid = False
    row = None
    column = None
    while not valid:  # Keep randomly choosing targets until a valid one is chosen
        row = randint(0, len(gamestate["MyBoard"]) - 1)  # Randomly pick a row
        column = randint(0, len(gamestate["MyBoard"][0]) - 1)  # Randomly pick a column
        if gamestate["OppBoard"][row][column] == "":  # If the target is sea that hasn't already been guessed...
            valid = True  # ...then the target is valid
    move = {"Row": chr(row + 65),
            "Column": (column + 1)}  # Set move equal to the valid target (convert the row to a letter 0->A, 1->B etc.)
    return move  # Return the move


# Returns a list of the lengths of your opponent's ships that haven't been sunk
def shipsStillAfloat(gamestate):
    afloat = []
    ships_removed = []
    for k in range(len(gamestate["Ships"])):  # For every ship
        afloat.append(gamestate["Ships"][k])  # Add it to the list of afloat ships
        ships_removed.append(False)  # Set its removed from afloat list to false
    for i in range(len(gamestate["OppBoard"])):
        for j in range(len(gamestate["OppBoard"][0])):  # For every grid on the board
            for k in range(len(gamestate["Ships"])):  # For every ship
                if str(k) in gamestate["OppBoard"][i][j] and not ships_removed[k]:  # If we can see the ship number on our opponent's board and we haven't already removed it from the afloat list
                    afloat.remove(gamestate["Ships"][k])  # Remove that ship from the afloat list (we can only see an opponent's ship number when the ship has been sunk)
                    ships_removed[k] = True  # Record that we have now removed this ship so we know not to try and remove it again
    return afloat  # Return the list of ships still afloat


# Returns a list of cells adjacent to the input cell that are free to be targeted (not including land)
def selectUntargetedAdjacentCell(row, column, oppBoard):
    adjacent = []  # List of adjacent cells
    if row > 0 and oppBoard[row - 1][column] == "":  # If there is a cell above
        adjacent.append((row - 1, column))  # Add to list of adjacent cells
    if row < len(oppBoard) - 1 and oppBoard[row + 1][column] == "":  # If there is a cell below
        adjacent.append((row + 1, column))  # Add to list of adjacent cells
    if column > 0 and oppBoard[row][column - 1] == "":  # If there is a cell left
        adjacent.append((row, column - 1))  # Add to list of adjacent cells
    if column < len(oppBoard[0]) - 1 and oppBoard[row][column + 1] == "":  # If there is a cell right
        adjacent.append((row, column + 1))  # Add to list of adjacent cells
    return adjacent


# Given a valid coordinate on the board returns it as a correctly formatted move
def translateMove(row, column):
    return {"Row": chr(row + 65), "Column": (column + 1)}
