from random import randint

from helper.battleships_helper_functions import deployRandomly, chooseRandomValidTarget
from strategy.strategy import Strategy


class RandomStrategy(Strategy):
    def name(self):
        return "random"

    def move(self, game_state):
        available = []
        for i in range(len(game_state["OppBoard"])):
            for j in range(len(game_state["OppBoard"][i])):
                if game_state["OppBoard"][i][j] == "":
                    available.append([i, j])
        if len(available) == 0:
            print(game_state)

        assert len(available) > 0

        move = available[randint(0, len(available) - 1)]

        return {
            "Row": chr(move[0] + 65),
            "Column": (move[1] + 1)
        }

    def place_boats(self, game_state):
        return deployRandomly(game_state)
