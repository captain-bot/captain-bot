class Strategy(object):
    def calculate_move(self, game_state):
        if game_state["Round"] == 0:
            return self.place_boats(game_state)
        else:
            return self.move(game_state)

    def move(self, game_state):
        raise NotImplementedError("Should have implemented this")

    def place_boats(self, game_state):
        raise NotImplementedError("Should have implemented this")

    def name(self):
        raise NotImplementedError("Should have implemented this")
