import sys

from game.battle_ships import BattleShips
from strategy.random_strategy import RandomStrategy


def main():
    if len(sys.argv) < 4:
        print("usage: python main.py [STRATEGY 1] [STRATEGY 2] [ROUNDS]")
        return

    game = BattleShips(strategy_by_name(sys.argv[1]), strategy_by_name(sys.argv[2]))
    game.play_game(int(sys.argv[3]))


def strategy_by_name(name):
    if name == "random":
        return RandomStrategy()

    return RandomStrategy()


if __name__ == "__main__":
    main()
