import time

from helper.battleships_helper_functions import deployShip
from copy import copy, deepcopy


class BattleShips:
    def __init__(self, player1_strategy, player2_strategy):
        self.ships = [5, 4, 3, 3, 2]
        self.round = 0
        self.player1 = {
            'strategy': player1_strategy,
            'state': [],
            "placement": [],
            "ships": [],
            'score': 0
        }
        self.player2 = {
            'strategy': player2_strategy,
            'state': [],
            "placement": [],
            "ships": [],
            'score': 0
        }

    def reset_round(self):
        self.round = 0
        self.reset_player(self.player1)
        self.reset_player(self.player2)
        return

    def reset_player(self, player):
        player["state"] = [
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
            ["", "", "", "", "", "", "", ""],
        ]
        player["placement"] = []
        player["ships"] = self.ships[:]

    def play_game(self, rounds):
        start_time = time.time()

        for i in range(rounds):
            self.play_round()

        print("Player 1 (" + self.player1["strategy"].name() + ") wins: " + str(self.player1["score"]) + "/" + str(
            rounds))
        print("Player 2 (" + self.player2["strategy"].name() + ") wins: " + str(self.player2["score"]) + "/" + str(
            rounds))
        print("It took: " + str(time.time() - start_time) + "s")

    def play_round(self):
        self.reset_round()

        placement1 = self.player1["strategy"].place_boats(self.game_state(self.player1, self.player2))
        placement2 = self.player2["strategy"].place_boats(self.game_state(self.player2, self.player1))
        self.initial_placement(self.player1, placement1["Placement"])
        self.initial_placement(self.player2, placement2["Placement"])

        while True:
            self.round += 1
            # for i in self.player1["state"]:
            #     print(i)
            # print("Round: " + str(self.round))

            move1 = self.player1["strategy"].move(self.game_state(self.player1, self.player2))
            # print(move1)
            self.make_move(self.player2, move1)
            if self.test_if_lost(self.player2):
                self.make_winner(self.player1)
                return
            # print(self.player2)
            move2 = self.player2["strategy"].move(self.game_state(self.player2, self.player1))
            # print(move2)
            self.make_move(self.player1, move2)
            # print(self.player1)
            if self.test_if_lost(self.player1):
                self.make_winner(self.player2)
                return

    def make_winner(self, player):
        player["score"] += 1

    def test_if_lost(self, player):
        for i in player["ships"]:
            if i > 0:
                return False
        return True

    def initial_placement(self, player, placement):
        player["placement"] = placement
        for i in range(len(placement)):
            deployShip(
                ord(placement[i]["Row"]) - 65,
                placement[i]["Column"] - 1,
                player["state"],
                self.ships[i],
                placement[i]["Orientation"],
                i
            )
        return

    def make_move(self, player, move):
        row = ord(move["Row"]) - 65
        col = move["Column"] - 1
        if player["state"][row][col] == "":
            player["state"][row][col] = "M"
        elif player["state"][row][col] == "L":
            player["state"][row][col] = "LM"
        elif player["state"][row][col].isdigit():
            ship = int(player["state"][row][col])
            player["ships"][ship] -= 1
            if player["ships"][ship] == 0:
                i = ord(player["placement"][ship]["Row"]) - 65
                j = player["placement"][ship]["Column"] - 1
                length = self.ships[ship]
                if player["placement"][ship]["Orientation"] == "V":
                    for l in range(length):
                        player["state"][i + l][j] = "S" + str(ship)
                else:
                    for l in range(length):
                        player["state"][i][j + l] = "S" + str(ship)
            else:
                player["state"][row][col] = "H" + player["state"][row][col]

    def mask_state(self, state):
        s = deepcopy(state)
        for i in range(len(s)):
            for j in range(len(s[i])):
                if len(s[i][j]) == 2:
                    if s[i][j][0] == "H":
                        s[i][j] = s[i][j][1]
                elif s[i][j].isdigit():
                    s[i][j] = ''
        return s

    def game_state(self, me, opponent, status="RUNNING"):
        return {
            "Ships": self.ships,
            "IsMover": True,
            "Round": self.round,
            "MyScore": me["score"],
            "OppScore": opponent["score"],
            "ResponseDeadline": 0,
            "GameStatus": status,
            "MyBoard": me["state"],
            "OppBoard": self.mask_state(opponent["state"]),
            "GameId": "Not implemented",
            "OpponentId": "Not implemented"
        }
